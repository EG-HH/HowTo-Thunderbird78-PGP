# Repo einrichten

```
git clone https://codeberg.org/EG-HH/HowTo-Thunderbird78-PGP.git Thunderbird
cd Thunderbird
cp .githooks/post-checkout .git/hooks/post-checkout
cp .githooks/post-merge .git/hooks/post-merge
cp .githooks/post-commit .git/hooks/post-commit
git checkout master
```

# PDF erstellen

```
pdflatex Thunderbird.tex
pdflatex Thunderbird.tex
```

# HTML erstellen

```
htlatex Thunderbird "Thunderbird.cfg"
```

# Copyright

Diese Anleitung ist gemeinfrei nach [Creative Commons CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de). Davon ausgenommen sind die Teile der Bildschirmfoto, die Mozilla Thunderbird und Enigmail zeigen, welche unter der [Mozilla Public License, Version 2.0](https://www.mozilla.org/en-US/MPL/2.0/) lizensiert sind. Mozilla, Thunderbird und das Thunderbird Logo sind [Marken der Mozilla Foundation](https://www.mozilla.org/en-US/foundation/trademarks/list/). Außerdem ausgenommen ist der verwendete CCS-Style [dark.css](dark.css) von [kognise](https://github.com/kognise), der unter der [MIT Lizenz](MIT.txt) steht.
